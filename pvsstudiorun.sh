#!/bin/sh -x

# Sample shell script that runs PVS-Studio on a git repository/module from kdesrc-build.

# Usage:
# pvsstudiorun.sh /home/user/kde/src/kcalc

MODULE_PATH=$1
MODULE_FULL_PATH=$(realpath $MODULE_PATH)
MODULE_NAME=$(basename $MODULE_FULL_PATH)
SRC_DIRECTORY_PATH=$(dirname $MODULE_FULL_PATH)
KDESRC_DIRECTORY_PATH=$(dirname $SRC_DIRECTORY_PATH)
PVSSTUDIOANALYZER_PATH=$(which pvs-studio-analyzer)
PVSSTUDIO_BIN_PATH=$(dirname $PVSSTUDIOANALYZER_PATH)
echo $KDESRC_DIRECTORY_PATH

rm -rf $KDESRC_DIRECTORY_PATH/build-pvs/$MODULE_NAME && mkdir -p $KDESRC_DIRECTORY_PATH/build-pvs/$MODULE_NAME && cd $KDESRC_DIRECTORY_PATH/build-pvs/$MODULE_NAME

export CMAKE_MODULE_PATH=$KDESRC_DIRECTORY_PATH/usr/lib64/cmake:$KDESRC_DIRECTORY_PATH/usr/lib/cmake
export PKG_CONFIG_PATH=$KDESRC_DIRECTORY_PATH/usr/lib/pkgconfig
export CMAKE_PREFIX_PATH=$KDESRC_DIRECTORY_PATH/usr
export LD_LIBRARY_PATH=$KDESRC_DIRECTORY_PATH/usr/lib
export XDG_DATA_DIRS=$KDESRC_DIRECTORY_PATH/usr/share:/usr/share/plasma:/usr/local/share:/usr/share
export PATH=$KDESRC_DIRECTORY_PATH/usr/bin:$KDESRC_DIRECTORY_PATH/src/kdesrc-build:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games

cmake -B . -S $KDESRC_DIRECTORY_PATH/src/$MODULE_NAME -G Kate\ -\ Ninja -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=ON -DCMAKE_BUILD_TYPE=Debug -DBUILD_WITH_QT6=ON -DCMAKE_CXX_FLAGS:STRING=-pipe -DCMAKE_INSTALL_PREFIX=$KDESRC_DIRECTORY_PATH/usr

$PVSSTUDIO_BIN_PATH/pvs-studio-analyzer trace -- ninja -j `nproc`
$PVSSTUDIO_BIN_PATH/pvs-studio-analyzer analyze -j `nproc` --disableLicenseExpirationCheck -o PVS-Studio.log
$PVSSTUDIO_BIN_PATH/plog-converter -a GA:1,2 -d V567,V1042,V1053,V1044 -t tasklist -o report.tasks PVS-Studio.log

kate report.tasks &
