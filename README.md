# kdesrc-build-extra

Extra modules for kdesrc-build

# HOWTO

kdesrc-build-extra does not work without kdesrc-build.

Install kdesrc-build https://community.kde.org/Get_Involved/development .

```
# sudo <PackageManager> <install> git gitk git-gui build-essential cmake dialog # perl perl-IPC-Cmd perl-MD5 perl-FindBin # other goodies (e.g. mc, $EDITOR, emacs-nox, krusader, qtcreator, VS code etc.).
git config --global user.name "Your Name"
git config --global user.email "you@email.com"
rm ~/.config/kdesrc-buildrc
mkdir -p ~/kde/src
cd ~/kde/src
git clone https://invent.kde.org/sdk/kdesrc-build.git
git clone https://invent.kde.org/nmariusp/kdesrc-build-extra.git
cd kdesrc-build
./kdesrc-build --version
./kdesrc-build --initial-setup
less ~/.bashrc
source ~/.bashrc
echo $PATH
echo include $HOME/kde/src/kdesrc-build-extra/qt5-all-include >> ~/.config/kdesrc-buildrc
$EDITOR ~/.config/kdesrc-buildrc
```

In this configuration file keep everything as is plus make the following changes:
```
cmake-options -DCMAKE_BUILD_TYPE=Debug
num-cores `numproc`
num-cores-low-mem `numproc - 1`
stop-on-failure false
```

# If you build Qt

You also want to build qtwebengine. Do something like:
```
__VERSION_OF_QT=$(grep MODULE_VERSION ~/kde/src/Qt5/qtbase/.qmake.conf)
echo $__VERSION_OF_QT
sed -i "s/MODULE_VERSION = 5.15.11/$__VERSION_OF_QT/" ~/kde/src/Qt5/qtwebengine/.qmake.conf
```

Edit ~/kde/src/kdesrc-build/qt5-build-include. In section "use-modules" append "qtwebengine". In section "configure-flags" append " -skip qt3d -skip qtcanvas3d -skip qtdatavis3d -skip qtquick3d".
