#!/bin/sh

# Sample shell script that runs PVS-Studio on a git repository/module from kdesrc-build.

# Usage: cd ~/kde/src/games ; for d in *; do  ~/kde/src/kdesrc-build-extra/pvsstudiorunner.sh games/$d; done |& tee ~/a.txt

# Could be "graphics/kxstitch" or "kxstitch". Depending on kdesrc-build directory-layout.
MODULE_PATH_RELATIVE_TO_SRC_DIR=$1

rm -rf $HOME/kde/build-pvs/$MODULE_PATH_RELATIVE_TO_SRC_DIR && mkdir -p $HOME/kde/build-pvs/$MODULE_PATH_RELATIVE_TO_SRC_DIR && cd $HOME/kde/build-pvs/$MODULE_PATH_RELATIVE_TO_SRC_DIR

export CMAKE_MODULE_PATH=$HOME/kde/usr/lib64/cmake:$HOME/kde/usr/lib/cmake
export PKG_CONFIG_PATH=$HOME/kde/usr/lib/pkgconfig
export CMAKE_PREFIX_PATH=$HOME/kde/usr
export LD_LIBRARY_PATH=$HOME/kde/usr/lib
export XDG_DATA_DIRS=$HOME/kde/usr/share:/usr/share/plasma:/usr/local/share:/usr/share
export PATH=$HOME/kde/usr/bin:$HOME/kde/src/kdesrc-build:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games

cmake -B . -S $HOME/kde/src/$MODULE_PATH_RELATIVE_TO_SRC_DIR -G Unix\ Makefiles -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=ON -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_FLAGS:STRING=-pipe -DCMAKE_INSTALL_PREFIX=$HOME/kde/usr

pvs-studio-analyzer trace -- make -j `nproc`
pvs-studio-analyzer analyze -j `nproc` -o PVS-Studio.log
plog-converter -a GA:1,2 -d V567,V1042,V1053,V1044 -t tasklist -o report.tasks PVS-Studio.log
