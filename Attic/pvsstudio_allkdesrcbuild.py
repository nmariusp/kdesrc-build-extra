#!/usr/bin/env python3

# Pseudocode:
# variable dirOfThisFile = .
# variable srcDir = $dirOfThisFile/..
# list <string> = relative path from $srcDir to all of the git repos that have .gitignore and CMakeLists.txt files.
# foreach file in files dirOfThisFile/pvsstudiorunner.sh $file
