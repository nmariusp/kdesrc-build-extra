#!/bin/bash

set -x
set -u
#set -e

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

rm -rf $SCRIPT_DIR/work && mkdir -p $SCRIPT_DIR/work/kde/src && cd $SCRIPT_DIR/work/kde/src
git clone --depth 1 https://invent.kde.org/sdk/kdesrc-build.git
# cd kdesrc-build ; ./kdesrc-build --initial-setup
cp $SCRIPT_DIR/Automation/kdesrc-buildrc $SCRIPT_DIR/work/kde/
sed -i 's=~='"$SCRIPT_DIR"'/work=g' $SCRIPT_DIR/work/kde/kdesrc-buildrc
sed -i 's=num-cores 4=num-cores '"$(nproc)"'=g' $SCRIPT_DIR/work/kde/kdesrc-buildrc
sed -i 's=directory-layout flat=directory-layout invent=g' $SCRIPT_DIR/work/kde/kdesrc-buildrc
