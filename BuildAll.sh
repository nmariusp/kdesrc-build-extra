#!/bin/bash

set -x
set -u
#set -e

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd $SCRIPT_DIR/work/kde/src/kdesrc-build
./kdesrc-build --rc-file=$SCRIPT_DIR/work/kde/kdesrc-buildrc --debug |& tee $SCRIPT_DIR/work/kde/stdoutandstderr.txt
